/**
 * @file fs_utils.h
 *
 *  Created on: Dec 6, 2023
 *  @author Mark Giebler
 *
 *  @brief Derived from the Atari UnoCart 2600 project code.
 *  https://github.com/robinhedwards/UnoCart-2600
 */

#ifndef INC_FS_UTILS_H_
#define INC_FS_UTILS_H_


// These next set of macros can be overridden by declaring them in main.h
// and including main.h before this header file.
#ifndef ENABLE_FS_TESTS
/** Set to 1 to enable file system tests. */
#define ENABLE_FS_TESTS	0
#endif

#ifndef LONG_FILENAME_LEN
/** _mg_ Made long file name storage configurable. Must be less than or equal to _MAX_LFN */
#define LONG_FILENAME_LEN	(80)
#endif

#ifndef SHORT_FILENAME_LEN
/** Application specific short file name length. Typically in the 8.3 naming format range. */
#define SHORT_FILENAME_LEN	(12)
#endif

#ifndef NUM_DIR_ITEMS
#define NUM_DIR_ITEMS	80	//!< Number of entities in a directory.
#endif

#define FS_NO_TRACE	(0)		//!< No trace data when reading directory
#define FS_TRACE	(1)		//!< Output trace data when reading directory


/// _mg_ Brought in long filename support from CubeMx FatFs.
/// Define for long file name
#define FATFS_LONG_NAME_ENABLE
#ifdef  FATFS_LONG_NAME_ENABLE
#define THE_FILENAME long_filename
#else
#define THE_FILENAME filename
#endif

/**
 * @brief Define a File System entity structure.
 * Entity may be a file or a directory.
 */
typedef struct {
	char isDir;			//!< 1 if entity is a directory, 0 if a file.
	char filename[SHORT_FILENAME_LEN+2];		//!< Abbreviated name of the entity.
	char long_filename[LONG_FILENAME_LEN+2];	//!< long name of entity. Todo: _mg_ make this a linked list of variable length strings? Dynamically allocate?
} DIR_ENTRY;


/* ******************* Public prototypes *******************************/
int test_fs(int ledBlink);

int fs_entry_compare(const void* p1, const void* p2);
char *fs_get_filename_ext(char *filename);
int fs_read_directory(char *path, int debugTrace);
DIR_ENTRY* fs_get_dir_entries(void);
int fs_get_number_dir_entries(void);

int fs_default_filename_filter(char* filename);
void fs_set_filename_filter(int (*fcn)(char*));

int fs_file_size(char * path);
uint32_t* fs_statistic(void);
int fs_format_exFAT(void);

#endif /* INC_FS_UTILS_H_ */
