/**
 * @file debug_helper.h
 *
 *  Created on: Dec 6, 2010
 *  @author mgiebler
 */

#ifndef INC_DEBUG_HELPER_H_
#define INC_DEBUG_HELPER_H_

// -----------------------------------------------------
// Mark's debug macro helpers
// -----------------------------------------------------

/// @brief G_DEBUG() - use to enclose debug-only code that can be disabled by redefining this macro.
/// 	Put any debug-only code inside the parenthesis (represented by 'code') in below macro.
///  	'code' can be multiple statements on multiple lines.
/// 	Comment out next line to disable all debug code.
#define G_DEBUG(code)  {code;}
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that discards 'code'.
#define G_DEBUG(code) {;}
#define G_DEBUG_LEVEL_SETTING	0	/* disabled debug level ** DO NOT CHANGE THIS ** */
#else
#define G_DEBUG_ENABLED
#ifndef G_DEBUG_LEVEL_SETTING
#define G_DEBUG_LEVEL_SETTING	3	/* set higher to enable more verbose debug output ** Change this to set verbose level ** */
#endif
#endif

//! @brief 		use G_COMMENT_OUT() for commenting out blocks of code with embedded comments.
#define G_COMMENT_OUT(...)
//! @brief 		use G_COMMENT_OUT_N() to temporarily re-enable a block of code that was commented out by G_COMMENT_OUT(...) macro
#define G_COMMENT_OUT_N(code_args...) code_args

//! @brief		Macro for printf type outputting debug code.
#define G_DEBUG_PRINT(code)	G_DEBUG(code)
//#define G_DEBUG_PRINT(code) {;}



/* *********************** Application Specific Debug Code Block control macros **************************************************/

//#define G_DEBUG_LED_TRACE_CART_START(code)	G_DEBUG(code)
#define G_DEBUG_LED_TRACE_CART_START(code) {;}



#endif /* INC_DEBUG_HELPER_H_ */

