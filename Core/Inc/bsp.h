/**
 * @file bsp.h
 *
 *  Created on: Dec 2, 2023
 *  @author mgiebler
 */

#ifndef INC_BSP_H_
#define INC_BSP_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#if defined (STM32F767xx)
#include "stm32f7xx_hal.h"


typedef enum
{
	LED1 = 0,
	LED_GREEN = LED1,
	LED2 = 1,
	LED_BLUE = LED2,
	LED3 = 2,
	LED_RED = LED3
}Led_TypeDef;

typedef enum
{
	BUTTON_USER = 0,
	/* Alias */
	BUTTON_KEY = BUTTON_USER
}Button_TypeDef;

typedef enum
{
	BUTTON_MODE_GPIO = 0,
	BUTTON_MODE_EXTI = 1
}ButtonMode_TypeDef;


/** @addtogroup STM32F7XX_NUCLEO_144_LOW_LEVEL_LED
* @{
*/
#define LEDn                                    3

#define LED1_PIN                                GPIO_PIN_0
#define LED1_GPIO_PORT                          GPIOB
#define LED1_GPIO_CLK_ENABLE()                  __HAL_RCC_GPIOB_CLK_ENABLE()
#define LED1_GPIO_CLK_DISABLE()                 __HAL_RCC_GPIOB_CLK_DISABLE()

#define LED2_PIN                                GPIO_PIN_7
#define LED2_GPIO_PORT                          GPIOB
#define LED2_GPIO_CLK_ENABLE()                  __HAL_RCC_GPIOB_CLK_ENABLE()
#define LED2_GPIO_CLK_DISABLE()                 __HAL_RCC_GPIOB_CLK_DISABLE()

#define LED3_PIN                                GPIO_PIN_14
#define LED3_GPIO_PORT                          GPIOB
#define LED3_GPIO_CLK_ENABLE()                  __HAL_RCC_GPIOB_CLK_ENABLE()
#define LED3_GPIO_CLK_DISABLE()                 __HAL_RCC_GPIOB_CLK_DISABLE()

#define LEDx_GPIO_CLK_ENABLE(__INDEX__)   do { if((__INDEX__) == 0) {__HAL_RCC_GPIOB_CLK_ENABLE();} else\
																																		{__HAL_RCC_GPIOB_CLK_ENABLE();   }} while(0)
#define LEDx_GPIO_CLK_DISABLE(__INDEX__)  do { if((__INDEX__) == 0) {__HAL_RCC_GPIOB_CLK_DISABLE();} else\
																																		{__HAL_RCC_GPIOB_CLK_DISABLE();   }} while(0)
/**
* @}
*/


/** @addtogroup STM32F7XX_NUCLEO_144_LOW_LEVEL_BUTTON
* @{
*/
#define BUTTONn                              1

/**
 * @brief Key push-button
 */
#define USER_BUTTON_PIN                          GPIO_PIN_13
#define USER_BUTTON_GPIO_PORT                    GPIOC
#define USER_BUTTON_GPIO_CLK_ENABLE()            __HAL_RCC_GPIOC_CLK_ENABLE()
#define USER_BUTTON_GPIO_CLK_DISABLE()           __HAL_RCC_GPIOC_CLK_DISABLE()
#define USER_BUTTON_EXTI_LINE                    GPIO_PIN_13
#define USER_BUTTON_EXTI_IRQn                    EXTI15_10_IRQn

#define BUTTONx_GPIO_CLK_ENABLE(__INDEX__)      USER_BUTTON_GPIO_CLK_ENABLE()
#define BUTTONx_GPIO_CLK_DISABLE(__INDEX__)     USER_BUTTON_GPIO_CLK_DISABLE()

/* Aliases */
#define KEY_BUTTON_PIN                       USER_BUTTON_PIN
#define KEY_BUTTON_GPIO_PORT                 USER_BUTTON_GPIO_PORT
#define KEY_BUTTON_GPIO_CLK_ENABLE()         USER_BUTTON_GPIO_CLK_ENABLE()
#define KEY_BUTTON_GPIO_CLK_DISABLE()        USER_BUTTON_GPIO_CLK_DISABLE()
#define KEY_BUTTON_EXTI_LINE                 USER_BUTTON_EXTI_LINE
#define KEY_BUTTON_EXTI_IRQn                 USER_BUTTON_EXTI_IRQn

/**
* @}
*/

/** @defgroup STM32F7XX_NUCLEO_144_LOW_LEVEL_Exported_Functions LOW LEVEL Exported Functions
* @{
*/
uint32_t		BSP_GetVersion(void);
void			BSP_LED_Init(Led_TypeDef Led);
void			BSP_LED_DeInit(Led_TypeDef Led);
void			BSP_LED_On(Led_TypeDef Led);
void			BSP_LED_Off(Led_TypeDef Led);
void			BSP_LED_Toggle(Led_TypeDef Led);
void			BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef ButtonMode);
void			BSP_PB_DeInit(Button_TypeDef Button);
uint32_t		BSP_PB_GetState(Button_TypeDef Button);

void			BSP_Green_Blink(uint32_t onTime, uint32_t cycleTime, uint32_t cycleCount);
void			BSP_Blue_Blink(uint32_t onTime, uint32_t cycleTime, uint32_t cycleCount);
void			BSP_Red_Blink(uint32_t onTime, uint32_t cycleTime, uint32_t cycleCount);

void			BSP_LED_Blink(Led_TypeDef LEDColor, uint32_t onTime, uint32_t cycleTime, uint32_t cycleCount);
void			BSP_Green_Blink_Forever(void);
void			BSP_Blue_Blink_Forever(void);
void			BSP_Red_Blink_Forever(void);
/**
* @}
*/

/* Declare Low Level inline functions for faster methods of turning LEDs on/off.
 * On F767 MB1137 PCBA LED is On when its port pin is High. */
__attribute__((always_inline)) __STATIC_INLINE void LL_LedGreen_On()
{
	LED1_GPIO_PORT->BSRR = (uint32_t)LED1_PIN;
}
__attribute__((always_inline)) __STATIC_INLINE void LL_LedGreen_Off()
{
	LED1_GPIO_PORT->BSRR = LED1_PIN << 16;
}

__attribute__((always_inline)) __STATIC_INLINE void LL_LedBlue_On()
{
	LED2_GPIO_PORT->BSRR = (uint32_t)LED2_PIN;
}
__attribute__((always_inline)) __STATIC_INLINE void LL_LedBlue_Off()
{
	LED2_GPIO_PORT->BSRR = LED2_PIN << 16;
}

__attribute__((always_inline)) __STATIC_INLINE void LL_LedRed_On()
{
	LED3_GPIO_PORT->BSRR = (uint32_t)LED3_PIN;
}
__attribute__((always_inline)) __STATIC_INLINE void LL_LedRed_Off()
{
	LED3_GPIO_PORT->BSRR = LED3_PIN << 16;
}


#endif	// #if defined (STM32F767xx)

#ifdef __cplusplus
}
#endif

#endif /* INC_BSP_H_ */
