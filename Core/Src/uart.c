/**
 * @file uart.c
 *
 *  Created on: Dec 5, 2023
 *  @author mgiebler
 *
 *  @brief Functions for UART IO.
 *
 *  Send string, character, etc.
 */

#include "main.h"		// main.h might have uart.c related macro overrides
#include "uart.h"		// must be included AFTER main.h for any macro overrides
#include <string.h>

// ================================================================================================================================

#define UART_TIMEOUT	(100ul)	/// UART send timeout in milliseconds. @todo _mg_ Make UART_TIMEOUT run-time configurable.

UART_HandleTypeDef *huartDirect;
uint8_t uBuffer[16];	//!< short buffer internal work that may need to persist longer due to background interrupts.

/**
 * @brief Setup which UART to use for direct UART IO functions.
 *
 * @param huart handle to the UART channel to use.
 *
 * @warning This MUST be called at least once before using the direct UART IO methods.
 *
 * @sa directIOReady()
 * @sa sendByte()
 * @sa sendString()
 * @sa sendStringN()
 */
void setupDirectIO(UART_HandleTypeDef *huart)
{
	huartDirect = huart;
}

/**
 * @brief Check if direct IO with a UART is setup and ready of IO.
 *
 * @return 1 if ready, 0 if not ready.
 *
 * @sa setupDirectIO()
 */
int directIOReady(void)
{
	return(huartDirect!= NULL);
}

/**
 * @brief Send one byte to the UART.
 * @note MUST call @ref setupDirectIO() before using this method.
 *
 * @param c
 */
void sendByte(uint8_t c)
{
	uBuffer[0] = c;
	sendStringN((const char*)uBuffer,1);
}

/**
 * @brief Send null terminated string via the UART
 * @note MUST call @ref setupDirectIO() before using this method.
 *
 * @param str null terminated string
 */
void sendString(const char* str)
{
	if(str && *str)
	{
		int len = strlen(str);
		HAL_UART_Transmit(huartDirect, (const uint8_t *)str, len, UART_TIMEOUT);
	}
}

/**
 * @brief Send string of specified length. String does not need to be null terminated.
 * @note MUST call @ref setupDirectIO() before using this method.
 *
 * @param str string data to send.
 * @param len number of bytes to send.
 */
void sendStringN(const char* str, uint32_t len)
{
	if(str && len)
	{
		HAL_UART_Transmit(huartDirect, (const uint8_t *)str, len, UART_TIMEOUT);
	}
}

/**
 * @brief Test output of data via UART.
 * Outputs the string: "\nsend UART Test!\n".
 *
 * Uart direct IO must be setup before calling this function.
 * @sa setupDirectIO()
 */
void sendUARTTest(void)
{
	if(directIOReady())
	{
		sendString("\nsend UART Test");
		sendByte('!');
		sendByte('\n');
	}
}


#include <stdarg.h> //for va_list var arg functions

/// @brief A printf function to use when stdio is not directed to a UART.
/// Uses vsnprintf() internally, then sends via @ref SendString().
///
/// @param fmt standard printf formated string
void _myprintf(const char *fmt, ...) {
	// this printf implementation takes more code space that using the clib printf.
	static char buffer[256];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buffer, sizeof(buffer), fmt, args);
	va_end(args);
	sendString(buffer);
}

// ============================================================================
// 						Function to support stdio
// ============================================================================

#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#define GETCHAR_PROTOTYPE int __io_getchar(void)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#define GETCHAR_PROTOTYPE int fgetc(FILE *f)
#endif

#include <sys/stat.h>
#include <sys/unistd.h>
#include <errno.h>


UART_HandleTypeDef *hTgtUart; /// Handle to which UART to target for stdio operations.

/**
 * @brief Set the UART to target stdio methods to.
 * @warning This MUST be called at least once before any stdio methods are called.
 *
 * @param huart pointer to the UART channel to use.
 *
 * @sa stdioNoBuffers()
 */
void stdioTarget(UART_HandleTypeDef *huart)
{
	hTgtUart = huart;
}

/**
 * @brief Check if stdio is configured and ready to send to a UART.
 *
 * @return 1 if ready, 0 if not ready.
 *
 * @ sa stdioTarget()
 */
int stdioReady(void)
{
	return(hTgtUart != NULL);
}

/**
 * @brief Setup libc for stdio without allocating buffers on the heap.
 *
 *@attention This function MUST be called before any stdio methods such as printf(), putchar(), etc.
 * Failure to do so will result in the stdio functions buffering data on the heap until a `\n` is sent.
 * Or a call to fflush(stdout) is performed.
 *
 * @note By NOT calling this function, stdio will be more resource intensive; CPU and RAM.
 *
 */
void stdioNoBuffers(void)
{
#if ENABLE_STDIO_SUPPORT
	// Disable buffers, such that minimum heap is used and IO occurs immediately.
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
#endif
}

/**
 * @brief Test that output via stdio methods are directed to UART.
 * Outputs the string: "\nstdio Test Output!\n".
 *
 * Before calling this function, you MUST configure which UART to target for stdio operations.
 * @sa stdioTarget
 */
void stdioTestOutput(void)
{
#if (ENABLE_STDIO_OUT_TEST == 1)
	if(stdioReady())
	{
		printf("\nstdio Test Output");
		putchar('!');
		fflush(stdout);		// test fflush if running in buffered IO mode to get output before \n sent.
		putchar('\n');
	}
#endif
}

/**
 * @brief Test stdio input methods are via the UART.
 *
 * This will block until user enters data followed by a newline.
 * The user input will be echoed back.
 *
 * Before calling this function, you MUST configure which UART to target for stdio operations.
 * @sa stdioTarget
 *
 * @warning Do not enter more than 79 characters.
 */
void stdioTestInput(void)
{
#if (ENABLE_STDIO_IN_TEST == 1)
	char buff[80];
	if(stdioReady())
	{
		puts("\nstdio Input? ");
		fflush(stdout);		// this is needed if running in buffered IO mode to get output before \n sent.
		gets(buff);
		printf("Rcvd: %s\n",buff);
	}
#endif
}


#if ENABLE_STDIO_SUPPORT
/**
* @brief  For stdio output support.
*
* Called by _write() in syscalls.c. For puts(), putchar(), printf(), etc.
*
* @verbatim
* 	This code is like various online examples:
*	https://forum.digikey.com/t/easily-use-printf-on-stm32/20157
*	https://gist.github.com/glegrain/ca92f631e578450a933c67ac3497b4df
*
*	This example has input (scanf) as well:
*	https://shawnhymel.com/1873/how-to-use-printf-on-stm32/
*
* @endverbatim
*
* What those examples DON'T tell you is that you will only get the output after a newline is sent.
* To get direct IO without buffering, call our @ref stdio_noBuffers() method first.
*
* @param  ch character to send
* @retval character sent
*
* Before calling this function, you MUST configure which UART to target for stdio operations.
* @sa stdioTarget
*/
PUTCHAR_PROTOTYPE
{
	HAL_UART_Transmit(hTgtUart, (const uint8_t *)&ch, 1, UART_TIMEOUT);

	return ch;
}

/**
 * @brief For stdio input support.
 *
 * Called by _read() in syscalls.c.  For getchar(), scanf(), etc.
 *
 * @return -1 if no input, else the character.
 *
 * Before calling this function, you MUST configure which UART to target for stdio operations.
 * @sa stdioTarget
 */
GETCHAR_PROTOTYPE
{
	HAL_StatusTypeDef hstatus;
	uint8_t rxBuf[4];
	hstatus = HAL_UART_Receive(hTgtUart, (uint8_t*) rxBuf, 1, HAL_MAX_DELAY);
	if (hstatus == HAL_OK)
		return rxBuf[0];

	return -1;
}

/**
 * @brief Overload the function in syscalls.c.
 * This is a little more efficient by calling the HAL directly instead of via PUTCHAR_PROTOTYPE.
 *
 * @param file
 * @param ptr
 * @param len
 * @return len or -1 if error.
 *
 * Before calling this function, you MUST configure which UART to target for stdio operations.
 * @sa stdioTarget
 */
int _write(int fd, char *ptr, int len)
{
	HAL_StatusTypeDef hres;
	if (fd == STDOUT_FILENO || fd == STDERR_FILENO)
	{
		hres = HAL_UART_Transmit(hTgtUart, (uint8_t*) ptr, len, UART_TIMEOUT);
		if (hres == HAL_OK)
			return len;
		else
			return EIO;
	}
	errno = EBADF;
	return -1;
}

/**
 * @brief Overload the function in syscalls.c to get input from the UART.
 * @warning This blocks FOREVER until input is received!
 *
 * @param fd
 * @param ptr
 * @param len
 * @return Character received.
 *
 * Before calling this function, you MUST configure which UART to target for stdio operations.
 * @sa stdioTarget
 */
int _read(int fd, char *ptr, int len) {
	HAL_StatusTypeDef hstatus;

	if (fd == STDIN_FILENO) {
		hstatus = HAL_UART_Receive(hTgtUart, (uint8_t*) ptr, 1, HAL_MAX_DELAY);
		if (hstatus == HAL_OK)
			return 1;
		else
			return EIO;
	}
	errno = EBADF;
	return -1;
}
#endif	// ENABLE_STDIO_SUPPORT

