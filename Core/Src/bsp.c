/**
 * @ file bsp.c
 *
 *  Created on: Dec 2, 2023
 *  @author mgiebler
 *
 * Copy a select set of BSP functions from stm32f7xx_nucleo_144.c
 */

#include "bsp.h"

#if defined (STM32F767xx)
/**
	* @brief STM32F7xx NUCLEO BSP Driver version number V1.0.2
	*/
#define __STM32F7xx_NUCLEO_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __STM32F7xx_NUCLEO_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __STM32F7xx_NUCLEO_BSP_VERSION_SUB2   (0x02) /*!< [15:8]  sub2 version */
#define __STM32F7xx_NUCLEO_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */
#define __STM32F7xx_NUCLEO_BSP_VERSION        ((__STM32F7xx_NUCLEO_BSP_VERSION_MAIN << 24)\
																						|(__STM32F7xx_NUCLEO_BSP_VERSION_SUB1 << 16)\
																						|(__STM32F7xx_NUCLEO_BSP_VERSION_SUB2 << 8 )\
																						|(__STM32F7xx_NUCLEO_BSP_VERSION_RC))



GPIO_TypeDef* GPIO_PORT[LEDn] = {LED1_GPIO_PORT, LED2_GPIO_PORT, LED3_GPIO_PORT};
const uint16_t GPIO_PIN[LEDn] = {LED1_PIN, LED2_PIN, LED3_PIN};

GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {USER_BUTTON_GPIO_PORT};
const uint16_t BUTTON_PIN[BUTTONn] = {USER_BUTTON_PIN};
const uint8_t BUTTON_IRQn[BUTTONn] = {USER_BUTTON_EXTI_IRQn};


/**
 * @brief  This method returns the STM32F7xx NUCLEO BSP Driver revision
 * @retval version: 0xXYZR (8bits for each decimal, R for RC)
 */
uint32_t BSP_GetVersion(void)
{
	return __STM32F7xx_NUCLEO_BSP_VERSION;
}

/**
 * @brief  Configures LED GPIO.
 * @param  Led: Specifies the Led to be configured.
 *   This parameter can be one of following parameters:
 *     @arg  LED1
 *     @arg  LED2
 *     @arg  LED3
 * @retval None
 */
void BSP_LED_Init(Led_TypeDef Led)
{
	GPIO_InitTypeDef  GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	LEDx_GPIO_CLK_ENABLE(Led);

	/* Configure the GPIO_LED pin */
	GPIO_InitStruct.Pin = GPIO_PIN[Led];
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

	HAL_GPIO_Init(GPIO_PORT[Led], &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
}

/**
 * @brief  DeInit LEDs.
 * @param  Led: LED to be de-init.
 *   This parameter can be one of the following values:
 *     @arg  LED1
 *     @arg  LED2
 *     @arg  LED3
 * @note Led DeInit does not disable the GPIO clock nor disable the Mfx
 * @retval None
 */
void BSP_LED_DeInit(Led_TypeDef Led)
{
	GPIO_InitTypeDef  gpio_init_structure;

	/* Turn off LED */
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
	/* DeInit the GPIO_LED pin */
	gpio_init_structure.Pin = GPIO_PIN[Led];
	HAL_GPIO_DeInit(GPIO_PORT[Led], gpio_init_structure.Pin);
}

/**
 * @brief  Turns selected LED On.
 * @param  Led: Specifies the Led to be set on.
 *   This parameter can be one of following parameters:
 *     @arg  LED1
 *     @arg  LED2
 *     @arg  LED3
 * @retval None
 */
void BSP_LED_On(Led_TypeDef Led)
{
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET);
}

/**
 * @brief  Turns selected LED Off.
 * @param  Led: Specifies the Led to be set off.
 *   This parameter can be one of following parameters:
 *     @arg  LED1
 *     @arg  LED2
 *     @arg  LED3
 * @retval None
 */
void BSP_LED_Off(Led_TypeDef Led)
{
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
}

/**
 * @brief  Toggles the selected LED.
 * @param  Led which @ref Led_TypeDef to toggle.
 *   This parameter can be one of following parameters:
 *     @arg  LED1 (@ref LED_GREEN)
 *     @arg  LED2 (@ref LED_BLUE)
 *     @arg  LED3 (@ref LED_RED)
 * @retval None
 */
void BSP_LED_Toggle(Led_TypeDef Led)
{
	HAL_GPIO_TogglePin(GPIO_PORT[Led], GPIO_PIN[Led]);
}

/**
 * @brief Blink the requested color LED with the specified parameters for the specified number of cycles.
 *
 * @param LEDColor which @ref Led_TypeDef to blink.
 *   This parameter can be one of following parameters:
 *     @arg  LED1 (@ref LED_GREEN)
 *     @arg  LED2 (@ref LED_BLUE)
 *     @arg  LED3 (@ref LED_RED)
 * @param onTime
 * @param cycleTime Time for one cycle (on time + off time)
 * @param cycleCount How many blink cycles
 */
void BSP_LED_Blink(Led_TypeDef LEDColor, uint32_t onTime, uint32_t cycleTime, uint32_t cycleCount)
{
	while(cycleCount--)
	{
		BSP_LED_On(LEDColor);
		HAL_Delay(onTime);
		BSP_LED_Off(LEDColor);
		HAL_Delay(cycleTime-onTime);
	}
}

/**
 * @brief Slowly toggle LD1 (Green) on/off forever.
 * Blink rate 0.333 Hz.
 * Requires interrupts to be enabled to use HAL_Delay().
 *
 */
void BSP_Green_Blink_Forever(void)
{
	__enable_irq();
	while(1)
	{
		BSP_LED_Blink(LED_GREEN,1500ul,3000ul,320000ul);
	}
}

/**
 * @brief Slowly toggle LD2 (Blue) on/off forever.
 * Blink rate 0.333 Hz.
 * Requires interrupts to be enabled to use HAL_Delay().
 *
 */
void BSP_Blue_Blink_Forever(void)
{
	__enable_irq();
	while(1)
	{
		BSP_LED_Blink(LED_BLUE,1500ul,3000ul,320000ul);
	}
}

/**
 * @brief Slowly toggle LD3 (Red) on/off forever.
 * Blink rate 0.333 Hz.
 * Requires interrupts to be enabled to use HAL_Delay().
 *
 */
void BSP_Red_Blink_Forever(void)
{
	__enable_irq();
	while(1)
	{
		BSP_LED_Blink(LED_RED,1500ul,3000ul,320000ul);
	}
}

/**
 * @brief  Configures Button GPIO and EXTI Line.
 * @param  Button: Specifies the Button to be configured.
 *   This parameter should be: BUTTON_USER
 * @param  ButtonMode: Specifies Button mode.
 *   This parameter can be one of following parameters:
 *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO
 *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *                            generation capability
 * @retval None
 */
void BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef ButtonMode)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the BUTTON Clock */
	BUTTONx_GPIO_CLK_ENABLE(Button);

	if(ButtonMode == BUTTON_MODE_GPIO)
	{
		/* Configure Button pin as input */
		GPIO_InitStruct.Pin = BUTTON_PIN[Button];
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
	}

	if(ButtonMode == BUTTON_MODE_EXTI)
	{
		/* Configure Button pin as input with External interrupt */
		GPIO_InitStruct.Pin = BUTTON_PIN[Button];
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
		HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);

		/* Enable and set Button EXTI Interrupt to the lowest priority */
		HAL_NVIC_SetPriority((IRQn_Type)(BUTTON_IRQn[Button]), 0x0F, 0x00);
		HAL_NVIC_EnableIRQ((IRQn_Type)(BUTTON_IRQn[Button]));
	}
}

/**
 * @brief  Push Button DeInit.
 * @param  Button: Button to be configured
 *   This parameter should be: BUTTON_USER
 * @note PB DeInit does not disable the GPIO clock
 * @retval None
 */
void BSP_PB_DeInit(Button_TypeDef Button)
{
		GPIO_InitTypeDef gpio_init_structure;

		gpio_init_structure.Pin = BUTTON_PIN[Button];
		HAL_NVIC_DisableIRQ((IRQn_Type)(BUTTON_IRQn[Button]));
		HAL_GPIO_DeInit(BUTTON_PORT[Button], gpio_init_structure.Pin);
}

/**
 * @brief  Returns the selected Button state.
 * @param  Button: Specifies the Button to be checked.
 *   This parameter should be: BUTTON_USER
 * @retval The Button GPIO pin value.
 */
uint32_t BSP_PB_GetState(Button_TypeDef Button)
{
	return HAL_GPIO_ReadPin(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}

#endif // #if defined (STM32F767xx)

