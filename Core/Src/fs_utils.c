/**
 * @file fs_utils.c
 *
 *  Created on: Dec 6, 2023
 *  @author mgiebler
 *
 *  @brief My file system Utilities.
 */


#include "main.h"		// main.h might have fs_utils.c related macro overrides
#include "fs_utils.h"	// must be included AFTER main.h for any macro overrides

#include "fatfs.h"		// support for FATxx file system
#include "bsp.h"		// for LED control and user button input
#include "uart.h"

#include <string.h>

#include "debug_helper.h"


// ================================================================================================================================

int scanDirDepth = 0;


/**
 * @brief Pointer to a filename filter that is used by @ref read_diectory()
 * to filter out undersiable files from the @ref DIR_ENTRY structures.
 *
 * Pointer can be set by calling @ref set_filename_filter().
 *
 * @param filename to run filter on
 * @return 1 if valid file, 0 if to ignore file.
 */
int (*fileFilterFnc)(char*)=fs_default_filename_filter;

/**
 * @brief Set an optional filename filter to use in @ref read_diectory()
 * to filter out undersiable files from the @ref DIR_ENTRY structures.
 *
 * Function must return 1 for valid files to include or 0 for undesirable files.
 *
 * @param fcn pointer to the filter function.
 */
void fs_set_filename_filter(int (*fcn)(char*))
{
	fileFilterFnc = fcn;
}

/**
 * @brief Default filename filter.
 * Does no filtering, always returns valid.
 *
 * @param filename - not used.
 * @return 1 (valid)
 */
int fs_default_filename_filter(char* filename)
{
	(void) filename;
	return 1;
}


__attribute__ ((section (".noinit"))) DIR_ENTRY fs_dir_entries[NUM_DIR_ITEMS];	//!< directory list. _mg_ put this in .noinit section.
// single FILINFO structure
__attribute__ ((section (".noinit"))) FILINFO fs_fInfo;				//!< _mg_ put this in .noinit section
//__attribute__ ((section (".noinit"))) char fs_lfn[_MAX_LFN + 1];   //!<  Buffer to store the LFN. _mg_ put this in .noinit section

int fs_num_dir_entries = 0; //!< Number entries in the @ref dir_entries array.


/**
 * @brief Get a pointer to an array of directory entries
 * found by @ref read_directory().
 *
 * @return pointer to @ref DIR_ENTRY array @ref fs_dir_entries
 */
DIR_ENTRY* fs_get_dir_entries(void)
{
	return fs_dir_entries;
}

/**
 * @brief Get the number of entries in the @ref fs_dir_entries array
 * found by @ref read_directory().
 *
 * @return the number of entries.
 */
int fs_get_number_dir_entries(void)
{
	return fs_num_dir_entries;
}

/**
 * @brief Compare two file system entities
 *
 * @param p1
 * @param p2
 * @return	return 0 if equal, non-zero if different.
 */
int fs_entry_compare(const void* p1, const void* p2)
{
	DIR_ENTRY* e1 = (DIR_ENTRY*)p1;
	DIR_ENTRY* e2 = (DIR_ENTRY*)p2;
	if (e1->isDir && !e2->isDir) return -1;
	else if (!e1->isDir && e2->isDir) return 1;
	else return strcasecmp(e1->long_filename, e2->long_filename);
}

/**
 * @brief return a pointer to the first extension found in a filename
 * searching from the end of string to the beginning.
 *
 * @param filename
 * @return pointer to start of the extension string, or pointer to "" if no extension found.
 */
char *fs_get_filename_ext(char *filename) {
	char *dot = strrchr(filename, '.');
	if(!dot || dot == filename)
		return "";
	return dot + 1;
}


/**
 * @brief Read the contents of a directory and populate the
 * global array of @ref DIR_ENTRY structures @ref fs_dir_entries.
 *
 * Build Option: List out directory list via UART.
 *
 * @param path  null terminated string of the directory path. (POSIX format)
 * @param debugTrace 1 if trace output to uart/stdio. 0 if no trace.
 * @return 1 if OK, 0 if not.
 *
 * Uses internal global structure: @ref fs_fInfo.
 */
int fs_read_directory(char *path, int debugTrace)
{
	int ret = 0;
	fs_num_dir_entries = 0;
	DIR_ENTRY *dst = (DIR_ENTRY *)&fs_dir_entries[0];
	(void) debugTrace;
	G_DEBUG_PRINT(if(debugTrace) myprintf("__read_dir: %s\n",path);)
	FATFS FatFs;
	HAL_Delay(100);
	if (f_mount(&FatFs, "", 1) == FR_OK) {
		DIR dir;
		if (f_opendir(&dir, path) == FR_OK) {
			if (strlen(path))
			{	// not root directory, add pseudo ".." to go up a dir
				dst->isDir = 1;
				strcpy(dst->long_filename, "(GO BACK)");
				strcpy(dst->filename, "..");
				G_DEBUG_PRINT(if(debugTrace) myprintf("  Dir: ..\n");)
				fs_num_dir_entries++;
				dst++;
			}
			while (fs_num_dir_entries < NUM_DIR_ITEMS) {
				if (f_readdir(&dir, &fs_fInfo) != FR_OK || fs_fInfo.fname[0] == 0)
					break;
				if (fs_fInfo.fattrib & (AM_HID | AM_SYS))
					continue;
				dst->isDir = fs_fInfo.fattrib & AM_DIR ? 1 : 0;
#ifdef FATFS_LONG_NAME_ENABLE
				if (!dst->isDir)
				{
					if (!fileFilterFnc(fs_fInfo.fname[0] ? fs_fInfo.fname : fs_fInfo.altname))
					{
						G_DEBUG_PRINT(if(debugTrace) myprintf(" Skip: %s\n",fs_fInfo.fname);)
						continue;
					}
					G_DEBUG_PRINT(if(debugTrace) myprintf(" File: ");)
				}else
				{
					G_DEBUG_PRINT(if(debugTrace) myprintf("  Dir: ");)
				}
#else
				if (!dst->isDir)
				{
					if (!fileFilterFnc(fs_fInfo.fname))
					{
						G_DEBUG_PRINT(if(debugTrace) myprintf("Skip: %s\n",fs_fInfo.fname);)
						continue;
					}
					G_DEBUG_PRINT(if(debugTrace) myprintf(" File: ");)
				}else
				{
					G_DEBUG_PRINT(if(debugTrace) myprintf("  Dir: ");)
				}
#endif
				// copy file record to short name
				strncpy(dst->filename, fs_fInfo.fname,SHORT_FILENAME_LEN);
				dst->filename[SHORT_FILENAME_LEN]=0;
#ifdef FATFS_LONG_NAME_ENABLE
				if (fs_fInfo.fname[0]) {
					// copy long name
					strncpy(dst->long_filename, fs_fInfo.fname, LONG_FILENAME_LEN);
					dst->long_filename[LONG_FILENAME_LEN] = 0;
				}
				else
				{
					strncpy(dst->long_filename, fs_fInfo.altname, SHORT_FILENAME_LEN);
					dst->long_filename[SHORT_FILENAME_LEN] = 0;
				}

#else
					strcpy(dst->long_filename, fs_fInfo.fname);
#endif
					G_DEBUG_PRINT(if(debugTrace) myprintf("%s\n",dst->long_filename);)
				dst++;
				fs_num_dir_entries++;
			}
			f_closedir(&dir);
			qsort((DIR_ENTRY *)&fs_dir_entries[0], fs_num_dir_entries, sizeof(DIR_ENTRY), fs_entry_compare);
			ret = 1;
		}
		f_mount(0, "", 1);
	}
	return ret;
}


/**
 * @brief
 * Reentrant function to scan through all directories and dump directory listing out serial port.
 * Initial Caller must setup up curpath. e.g. curpath[0]=0.
 *
 * Max directory depth traversable may depend on stack size available,
 * CAUTION! Too deep and this may hard-fault crash if your stack isn't large enough.
 * A hard limit of 128 reentrant calls is imposed in this function.
 *
 * Serial port dump of directory objects happens in calls made to read_directory()
 *
 * If there is a failure, this calls @ref BSP_Red_Blink_Forever() and does not return.
 *
 * @param curPath path to the current directory to scan.
 * @return 1 if OK, 0 if error.
 */
int fs_scan_directories(char* curPath)
{
	int sel, this_dir_entries;
	int retval = 1;
	char* newPath;
	scanDirDepth++;
	myprintf("\n>>> Depth: %d",scanDirDepth);
	if(scanDirDepth>128)
	{
		myprintf("****** Directory Depth too deep!\n");
		scanDirDepth--;
		return 0;
	}
	newPath = malloc(_MAX_LFN + 1);
	if(newPath == NULL)
	{
		myprintf(" malloc FAIL!\n");
		scanDirDepth--;
		return 0;
	}
	newPath[_MAX_LFN]=0;
	myprintf("\n***  cd to: %s/ \n", curPath);
	if (!(retval=fs_read_directory(curPath,FS_TRACE)))
	{
		myprintf("CANT READ FS");
		goto cleanExit;
	}
	this_dir_entries = fs_num_dir_entries;
	myprintf("*** %d items of interest in %s/ \n", this_dir_entries, curPath);

	for( sel = 0; sel < this_dir_entries; sel++)
	{
		DIR_ENTRY *d = &fs_dir_entries[sel];
		if (d->isDir)
		{	// selection is a directory
			if (!strcmp(d->filename, ".."))
			{	// skip .. (parent directory)
				continue;
			}
			// go into directory
			strncpy(newPath, curPath, _MAX_LFN);
			strncat(newPath, "/", _MAX_LFN);
			strncat(newPath, d->THE_FILENAME, _MAX_LFN);
			if(!(retval=fs_scan_directories(newPath)))
				break;
			myprintf("\n<< back to: %s/ \n", curPath);
			// re-read the curPath to restore dir_entries[] to curPath
			if (!(retval=fs_read_directory(curPath,FS_NO_TRACE)))
			{
				myprintf("CANT READ FS");
				break;
			}
		}
	}
	cleanExit:
	free(newPath);
	scanDirDepth--;
	return retval;
}

/**
 * @brief Return the size of the request file.
 *
 * Uses internal global structure: @ref fs_fInfo.
 * Mounts and unmounts the fatFS.
 *
 * @param path - full path and filename of the file.
 * @return  size of file in bytes, or -1 if failure.
 */
int fs_file_size(char * path)
{
	FATFS FatFs;
	int file_size = -1;
	if ( f_mount(&FatFs, "", 1) == FR_OK) {
		if( f_stat(path, &fs_fInfo) == FR_OK)
			file_size = (int)fs_fInfo.fsize;
		f_mount(0, "", 1);
	}
	return file_size;
}

/**
 * @brief Get file system size statistics.
 * Populates internal uint32_t array of size data.
 *
 * Mounts and unmounts the FS.
 *
 * @return pointer to internal array of statistics, index:
 * 			- 0 - total FS size in kilobytes (KiB)
 * 			- 1 - used space in kilobytes
 * 			- 2 - remaining free space in kilobytes
 */
uint32_t* fs_statistic(void)
{
	FATFS FatFs;
	__attribute__ ((section (".noinit"))) static DWORD statistics[3];
	// init at function call to eliminate power on init time.
	statistics[0] = 0;
	statistics[1] = 0;
	statistics[2] = 0;
	if (f_mount(&FatFs, "", 1) == FR_OK) {
		uint32_t free_clusters, used_size, total_size, free_size;
		FATFS* getFreeFs;
		if (f_getfree("", &free_clusters, &getFreeFs) == FR_OK) {
			// Formula comes from ChaN's documentation
			total_size = (getFreeFs->n_fatent - 2) * getFreeFs->csize;
			free_size = free_clusters * getFreeFs->csize;
			used_size = total_size - free_size;
			// convert sizes to megabytes. (KiB)
			statistics[0] = (total_size / 2ul);
			statistics[1] = (used_size / 2ul);
			statistics[2] = (free_size / 2ul);
		}
		f_mount(0, "", 1);
	}
	return statistics;
}


/**
 * @brief Format the file system to exFAT.
 *
 * @return 1 if OK, 0 if failure.
 */
int fs_format_exFAT(void)
{
	BYTE work[512]; /* Work area (larger is better for processing time) */
	return f_mkfs( "", FM_EXFAT, 0, work, sizeof work) == FR_OK ? 1:0;
}

/**
 * @brief Test FS and dump file system info.
 * @author Mark Giebler
 *
 * This mounts the  file system (typically SD card).
 * Execute some read/write tests on the FS.
 * Then dump some directories if found. -> Enable macro: G_DEBUG_PRINT()
 *
 * Expects to find /read.txt file with some data in it.
 * Outputs first 30 bytes of contents to UART.
 *
 * Creates /write.txt file with some data in it.
 *
 * Uses file system functions from ST's FatFS library file: ff.c
 *
 * @param ledBlink if 0 then this returns when complete.
 * 	If 1 then this
 *  Does NOT return, blinks blue LED forever when complete OK.
 *  If there is an error with the FS, blinks red LED forever.
 *
 * @return 0 if OK, -1 if error encountered.
 *
 * @note If @ref ENABLE_FS_TESTS is NOT defined as 1, then this
 * function does nothing and ALWAYS returns 0.
 */
int test_fs(int ledBlink)
{
#if (ENABLE_FS_TESTS == 1)
	// Some variables for FatFs
	FATFS FatFs;	// Fat File System handle
	FIL fil;		// File handle
	FRESULT fres;	// Result after operations

	myprintf("\n\n~~~ File System tests by MarkG ~~~\n\n");

	// Get some statistics from the FS - this will unmount the FS.
	uint32_t* size_Stats = fs_statistic();
	myprintf("File System stats:\n%10lu KiB total drive space.\n%10lu KiB used.\n%10lu KiB available.\n",
			size_Stats[0], size_Stats[1], size_Stats[2]);

	// Open the file system
	fres = f_mount(&FatFs, "", 1); // 1=mount now
	if (fres != FR_OK) {
		myprintf("f_mount error (%i)\n", fres);
		if(ledBlink)
			BSP_Red_Blink_Forever();
		return -1;
	}

	// Read 30 bytes from "read.txt" on the SD card
	BYTE readBuf[30];

	myprintf("f_open: read.txt\n");
	fres = f_open(&fil, "read.txt", FA_READ);
	if (fres != FR_OK) {
		myprintf("f_open error (%i)\n", fres);
	}else
	{
		myprintf("Opened 'read.txt' for reading!\n");
		// We can either use f_read OR f_gets to get data out of files
		// f_gets is a wrapper on f_read that does some string formatting for us
		TCHAR* rres = f_gets((TCHAR*)readBuf, 30, &fil);
		if(rres != 0) {
			myprintf("Read string from 'read.txt' contents: %s\n", readBuf);
		} else {
			myprintf("f_gets error (%i)\n", fres);
		}
		f_close(&fil);
	}
	// -----------------------------------------------
	// try and write a file "write.txt"
	// -----------------------------------------------
	fres = f_open(&fil, "write.txt", FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
	if(fres == FR_OK) {
		myprintf("Opened 'write.txt' for writing\n");
	} else {
		myprintf("f_open error (%i)\r\n", fres);
	}

	// Copy in a string without the null termination
	memcpy((char*)readBuf, "A new file is made!", 19);
	UINT bytesWrote;
	fres = f_write(&fil, readBuf, 19, &bytesWrote);
	if(fres == FR_OK) {
		myprintf("Wrote %i bytes to 'write.txt'!\n", bytesWrote);
	} else {
		myprintf("f_write error (%i)\n", fres);
	}

	fres = f_close(&fil);
	if(fres == FR_OK) {
		myprintf("Closed file: write.txt.\n");
	}else{
		myprintf("f_close error (%i)\n", fres);
	}
	// check file size of created file
	myprintf("write.txt file size: %d Bytes\n",
			fs_file_size("write.txt"));
	// Done, so un-mount the drive
	f_mount(NULL, "", 0);
	myprintf("Drive Unmounted.\n");

	// -----------------------------------------------
	// dump some directories out the serial port
	// -----------------------------------------------
	myprintf("\n*** Start Dump of File System ***\n");
	char curPath[256];
	curPath[0] = 0;
	// Danger! Danger! This next function is reentrant, might hard-fault if deep directory structure is traversed and stack gets blown.
	fs_scan_directories(curPath);
	// Close the file system.
	f_mount(NULL, "", 0);
	myprintf("\n*** Done with Dump of File System ***\n");
	myprintf("\n\n~~~ File System tests FINISHED ~~~\n\n");
	if(ledBlink)
		BSP_Blue_Blink_Forever();
#else
	(void) ledBlink;
#endif	// ENABLE_FS_TESTS
	return 0;
}
