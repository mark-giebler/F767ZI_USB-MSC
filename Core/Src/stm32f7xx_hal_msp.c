/* USER CODE BEGIN Header */
/**
	******************************************************************************
	* @file         stm32f7xx_hal_msp.c
	* @brief        This file provides code for the MSP Initialization
	*               and de-Initialization codes.
	******************************************************************************
	* @attention
	*
	* Copyright (c) 2023 STMicroelectronics.
	* All rights reserved.
	*
	* This software is licensed under terms that can be found in the LICENSE file
	* in the root directory of this software component.
	* If no LICENSE file comes with this software, it is provided AS-IS.
	*
	******************************************************************************
	*/
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */
extern DMA_HandleTypeDef hdma_sdmmc1_rx;

extern DMA_HandleTypeDef hdma_sdmmc1_tx;

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN Define */

/* USER CODE END Define */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN Macro */

/* USER CODE END Macro */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* External functions --------------------------------------------------------*/
/* USER CODE BEGIN ExternalFunctions */

/* USER CODE END ExternalFunctions */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */
/**
	* Initializes the Global MSP.
	*/
void HAL_MspInit(void)
{
	/* USER CODE BEGIN MspInit 0 */

	/* USER CODE END MspInit 0 */

	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();

	/* System interrupt init*/

	/* USER CODE BEGIN MspInit 1 */

	/* USER CODE END MspInit 1 */
}

/**
* @brief ADC MSP Initialization
* This function configures the hardware resources used in this example
* @param hadc: ADC handle pointer
* @retval None
*/
void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hadc->Instance==ADC1)
	{
	/* USER CODE BEGIN ADC1_MspInit 0 */

	/* USER CODE END ADC1_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_ADC1_CLK_ENABLE();

		__HAL_RCC_GPIOC_CLK_ENABLE();
		/**ADC1 GPIO Configuration
		PC3     ------> ADC1_IN13
		*/
		GPIO_InitStruct.Pin = CN9_A2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(CN9_A2_GPIO_Port, &GPIO_InitStruct);

	/* USER CODE BEGIN ADC1_MspInit 1 */

	/* USER CODE END ADC1_MspInit 1 */
	}

}

/**
* @brief ADC MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hadc: ADC handle pointer
* @retval None
*/
void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{
	if(hadc->Instance==ADC1)
	{
	/* USER CODE BEGIN ADC1_MspDeInit 0 */

	/* USER CODE END ADC1_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_ADC1_CLK_DISABLE();

		/**ADC1 GPIO Configuration
		PC3     ------> ADC1_IN13
		*/
		HAL_GPIO_DeInit(CN9_A2_GPIO_Port, CN9_A2_Pin);

	/* USER CODE BEGIN ADC1_MspDeInit 1 */

	/* USER CODE END ADC1_MspDeInit 1 */
	}

}

/**
* @brief DAC MSP Initialization
* This function configures the hardware resources used in this example
* @param hdac: DAC handle pointer
* @retval None
*/
void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hdac->Instance==DAC)
	{
	/* USER CODE BEGIN DAC_MspInit 0 */

	/* USER CODE END DAC_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_DAC_CLK_ENABLE();

		__HAL_RCC_GPIOA_CLK_ENABLE();
		/**DAC GPIO Configuration
		PA4     ------> DAC_OUT1
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_4;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* USER CODE BEGIN DAC_MspInit 1 */

	/* USER CODE END DAC_MspInit 1 */
	}

}

/**
* @brief DAC MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hdac: DAC handle pointer
* @retval None
*/
void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{
	if(hdac->Instance==DAC)
	{
	/* USER CODE BEGIN DAC_MspDeInit 0 */

	/* USER CODE END DAC_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_DAC_CLK_DISABLE();

		/**DAC GPIO Configuration
		PA4     ------> DAC_OUT1
		*/
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_4);

	/* USER CODE BEGIN DAC_MspDeInit 1 */

	/* USER CODE END DAC_MspDeInit 1 */
	}

}

/**
* @brief ETH MSP Initialization
* This function configures the hardware resources used in this example
* @param heth: ETH handle pointer
* @retval None
*/
void HAL_ETH_MspInit(ETH_HandleTypeDef* heth)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(heth->Instance==ETH)
	{
	/* USER CODE BEGIN ETH_MspInit 0 */

	/* USER CODE END ETH_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_ETH_CLK_ENABLE();

		__HAL_RCC_GPIOC_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		__HAL_RCC_GPIOG_CLK_ENABLE();
		/**ETH GPIO Configuration
		PC1     ------> ETH_MDC
		PA1     ------> ETH_REF_CLK
		PA2     ------> ETH_MDIO
		PA7     ------> ETH_CRS_DV
		PC4     ------> ETH_RXD0
		PC5     ------> ETH_RXD1
		PB13     ------> ETH_TXD1
		PG11     ------> ETH_TX_EN
		PG13     ------> ETH_TXD0
		*/
		GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = RMII_TXD1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	/* USER CODE BEGIN ETH_MspInit 1 */

	/* USER CODE END ETH_MspInit 1 */
	}

}

/**
* @brief ETH MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param heth: ETH handle pointer
* @retval None
*/
void HAL_ETH_MspDeInit(ETH_HandleTypeDef* heth)
{
	if(heth->Instance==ETH)
	{
	/* USER CODE BEGIN ETH_MspDeInit 0 */

	/* USER CODE END ETH_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_ETH_CLK_DISABLE();

		/**ETH GPIO Configuration
		PC1     ------> ETH_MDC
		PA1     ------> ETH_REF_CLK
		PA2     ------> ETH_MDIO
		PA7     ------> ETH_CRS_DV
		PC4     ------> ETH_RXD0
		PC5     ------> ETH_RXD1
		PB13     ------> ETH_TXD1
		PG11     ------> ETH_TX_EN
		PG13     ------> ETH_TXD0
		*/
		HAL_GPIO_DeInit(GPIOC, RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin);

		HAL_GPIO_DeInit(GPIOA, RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin);

		HAL_GPIO_DeInit(RMII_TXD1_GPIO_Port, RMII_TXD1_Pin);

		HAL_GPIO_DeInit(GPIOG, RMII_TX_EN_Pin|RMII_TXD0_Pin);

	/* USER CODE BEGIN ETH_MspDeInit 1 */

	/* USER CODE END ETH_MspDeInit 1 */
	}

}

/**
* @brief QSPI MSP Initialization
* This function configures the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/
void HAL_QSPI_MspInit(QSPI_HandleTypeDef* hqspi)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hqspi->Instance==QUADSPI)
	{
	/* USER CODE BEGIN QUADSPI_MspInit 0 */

	/* USER CODE END QUADSPI_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_QSPI_CLK_ENABLE();

		__HAL_RCC_GPIOE_CLK_ENABLE();
		__HAL_RCC_GPIOF_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		/**QUADSPI GPIO Configuration
		PE2     ------> QUADSPI_BK1_IO2
		PF6     ------> QUADSPI_BK1_IO3
		PF8     ------> QUADSPI_BK1_IO0
		PF9     ------> QUADSPI_BK1_IO1
		PF10     ------> QUADSPI_CLK
		PB10     ------> QUADSPI_BK1_NCS
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_2;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
		HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_10;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
		HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
		HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_10;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USER CODE BEGIN QUADSPI_MspInit 1 */

	/* USER CODE END QUADSPI_MspInit 1 */
	}

}

/**
* @brief QSPI MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/
void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef* hqspi)
{
	if(hqspi->Instance==QUADSPI)
	{
	/* USER CODE BEGIN QUADSPI_MspDeInit 0 */

	/* USER CODE END QUADSPI_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_QSPI_CLK_DISABLE();

		/**QUADSPI GPIO Configuration
		PE2     ------> QUADSPI_BK1_IO2
		PF6     ------> QUADSPI_BK1_IO3
		PF8     ------> QUADSPI_BK1_IO0
		PF9     ------> QUADSPI_BK1_IO1
		PF10     ------> QUADSPI_CLK
		PB10     ------> QUADSPI_BK1_NCS
		*/
		HAL_GPIO_DeInit(GPIOE, GPIO_PIN_2);

		HAL_GPIO_DeInit(GPIOF, GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10);

		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_10);

	/* USER CODE BEGIN QUADSPI_MspDeInit 1 */

	/* USER CODE END QUADSPI_MspDeInit 1 */
	}

}

/**
* @brief RTC MSP Initialization
* This function configures the hardware resources used in this example
* @param hrtc: RTC handle pointer
* @retval None
*/
void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
	if(hrtc->Instance==RTC)
	{
	/* USER CODE BEGIN RTC_MspInit 0 */

	/* USER CODE END RTC_MspInit 0 */

	/** Initializes the peripherals clock
	*/
		PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
		PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
		if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
		{
			Error_Handler();
		}

		/* Peripheral clock enable */
		__HAL_RCC_RTC_ENABLE();
	/* USER CODE BEGIN RTC_MspInit 1 */

	/* USER CODE END RTC_MspInit 1 */
	}

}

/**
* @brief RTC MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hrtc: RTC handle pointer
* @retval None
*/
void HAL_RTC_MspDeInit(RTC_HandleTypeDef* hrtc)
{
	if(hrtc->Instance==RTC)
	{
	/* USER CODE BEGIN RTC_MspDeInit 0 */

	/* USER CODE END RTC_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_RTC_DISABLE();
	/* USER CODE BEGIN RTC_MspDeInit 1 */

	/* USER CODE END RTC_MspDeInit 1 */
	}

}

/**
* @brief SD MSP Initialization
* This function configures the hardware resources used in this example
* @param hsd: SD handle pointer
* @retval None
*/
void HAL_SD_MspInit(SD_HandleTypeDef* hsd)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
	if(hsd->Instance==SDMMC1)
	{
	/* USER CODE BEGIN SDMMC1_MspInit 0 */

	/* USER CODE END SDMMC1_MspInit 0 */

	/** Initializes the peripherals clock
	*/
		PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SDMMC1|RCC_PERIPHCLK_CLK48;
		PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
		PeriphClkInitStruct.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_CLK48;
		if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
		{
			Error_Handler();
		}

		/* Peripheral clock enable */
		__HAL_RCC_SDMMC1_CLK_ENABLE();

		__HAL_RCC_GPIOC_CLK_ENABLE();
		__HAL_RCC_GPIOD_CLK_ENABLE();
		/**SDMMC1 GPIO Configuration
		PC8     ------> SDMMC1_D0
		PC9     ------> SDMMC1_D1
		PC10     ------> SDMMC1_D2
		PC11     ------> SDMMC1_D3
		PC12     ------> SDMMC1_CK
		PD2     ------> SDMMC1_CMD
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
													|GPIO_PIN_12;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_2;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

		/* SDMMC1 DMA Init */
		/* SDMMC1_RX Init */
		hdma_sdmmc1_rx.Instance = DMA2_Stream3;
		hdma_sdmmc1_rx.Init.Channel = DMA_CHANNEL_4;
		hdma_sdmmc1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
		hdma_sdmmc1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_sdmmc1_rx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_sdmmc1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_sdmmc1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
		hdma_sdmmc1_rx.Init.Mode = DMA_PFCTRL;
		hdma_sdmmc1_rx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_sdmmc1_rx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
		hdma_sdmmc1_rx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_sdmmc1_rx.Init.MemBurst = DMA_MBURST_INC4;
		hdma_sdmmc1_rx.Init.PeriphBurst = DMA_PBURST_INC4;
		if (HAL_DMA_Init(&hdma_sdmmc1_rx) != HAL_OK)
		{
			Error_Handler();
		}

		__HAL_LINKDMA(hsd,hdmarx,hdma_sdmmc1_rx);

		/* SDMMC1_TX Init */
		hdma_sdmmc1_tx.Instance = DMA2_Stream6;
		hdma_sdmmc1_tx.Init.Channel = DMA_CHANNEL_4;
		hdma_sdmmc1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_sdmmc1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_sdmmc1_tx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_sdmmc1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_sdmmc1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
		hdma_sdmmc1_tx.Init.Mode = DMA_PFCTRL;
		hdma_sdmmc1_tx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_sdmmc1_tx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
		hdma_sdmmc1_tx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_sdmmc1_tx.Init.MemBurst = DMA_MBURST_INC4;
		hdma_sdmmc1_tx.Init.PeriphBurst = DMA_PBURST_INC4;
		if (HAL_DMA_Init(&hdma_sdmmc1_tx) != HAL_OK)
		{
			Error_Handler();
		}

		__HAL_LINKDMA(hsd,hdmatx,hdma_sdmmc1_tx);

		/* SDMMC1 interrupt Init */
		HAL_NVIC_SetPriority(SDMMC1_IRQn, 1, 0);
		HAL_NVIC_EnableIRQ(SDMMC1_IRQn);
	/* USER CODE BEGIN SDMMC1_MspInit 1 */

	/* USER CODE END SDMMC1_MspInit 1 */
	}

}

/**
* @brief SD MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hsd: SD handle pointer
* @retval None
*/
void HAL_SD_MspDeInit(SD_HandleTypeDef* hsd)
{
	if(hsd->Instance==SDMMC1)
	{
	/* USER CODE BEGIN SDMMC1_MspDeInit 0 */

	/* USER CODE END SDMMC1_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_SDMMC1_CLK_DISABLE();

		/**SDMMC1 GPIO Configuration
		PC8     ------> SDMMC1_D0
		PC9     ------> SDMMC1_D1
		PC10     ------> SDMMC1_D2
		PC11     ------> SDMMC1_D3
		PC12     ------> SDMMC1_CK
		PD2     ------> SDMMC1_CMD
		*/
		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
													|GPIO_PIN_12);

		HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);

		/* SDMMC1 DMA DeInit */
		HAL_DMA_DeInit(hsd->hdmarx);
		HAL_DMA_DeInit(hsd->hdmatx);

		/* SDMMC1 interrupt DeInit */
		HAL_NVIC_DisableIRQ(SDMMC1_IRQn);
		/* USER CODE BEGIN SDMMC1_MspDeInit 1 */

	/* USER CODE END SDMMC1_MspDeInit 1 */
	}

}

/**
* @brief TIM_Base MSP Initialization
* This function configures the hardware resources used in this example
* @param htim_base: TIM_Base handle pointer
* @retval None
*/
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{
	if(htim_base->Instance==TIM14)
	{
	/* USER CODE BEGIN TIM14_MspInit 0 */

	/* USER CODE END TIM14_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_TIM14_CLK_ENABLE();
	/* USER CODE BEGIN TIM14_MspInit 1 */

	/* USER CODE END TIM14_MspInit 1 */
	}

}

/**
* @brief TIM_Base MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param htim_base: TIM_Base handle pointer
* @retval None
*/
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{
	if(htim_base->Instance==TIM14)
	{
	/* USER CODE BEGIN TIM14_MspDeInit 0 */

	/* USER CODE END TIM14_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_TIM14_CLK_DISABLE();
	/* USER CODE BEGIN TIM14_MspDeInit 1 */

	/* USER CODE END TIM14_MspDeInit 1 */
	}

}

/**
* @brief UART MSP Initialization
* This function configures the hardware resources used in this example
* @param huart: UART handle pointer
* @retval None
*/
void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
	if(huart->Instance==USART3)
	{
	/* USER CODE BEGIN USART3_MspInit 0 */

	/* USER CODE END USART3_MspInit 0 */

	/** Initializes the peripherals clock
	*/
		PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3;
		PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
		if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
		{
			Error_Handler();
		}

		/* Peripheral clock enable */
		__HAL_RCC_USART3_CLK_ENABLE();

		__HAL_RCC_GPIOD_CLK_ENABLE();
		/**USART3 GPIO Configuration
		PD8     ------> USART3_TX
		PD9     ------> USART3_RX
		*/
		GPIO_InitStruct.Pin = STLK_RX_Pin|STLK_TX_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

		/* USART3 interrupt Init */
		HAL_NVIC_SetPriority(USART3_IRQn, 6, 0);
		HAL_NVIC_EnableIRQ(USART3_IRQn);
	/* USER CODE BEGIN USART3_MspInit 1 */

	/* USER CODE END USART3_MspInit 1 */
	}

}

/**
* @brief UART MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param huart: UART handle pointer
* @retval None
*/
void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
	if(huart->Instance==USART3)
	{
	/* USER CODE BEGIN USART3_MspDeInit 0 */

	/* USER CODE END USART3_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_USART3_CLK_DISABLE();

		/**USART3 GPIO Configuration
		PD8     ------> USART3_TX
		PD9     ------> USART3_RX
		*/
		HAL_GPIO_DeInit(GPIOD, STLK_RX_Pin|STLK_TX_Pin);

		/* USART3 interrupt DeInit */
		HAL_NVIC_DisableIRQ(USART3_IRQn);
	/* USER CODE BEGIN USART3_MspDeInit 1 */

	/* USER CODE END USART3_MspDeInit 1 */
	}

}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
