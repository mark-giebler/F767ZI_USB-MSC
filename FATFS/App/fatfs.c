/* USER CODE BEGIN Header */
/**
******************************************************************************
* @file   fatfs.c
* @brief  Code for fatfs applications
******************************************************************************
* @attention
*
* Copyright (c) 2023 STMicroelectronics.
* All rights reserved.
*
* This software is licensed under terms that can be found in the LICENSE file
* in the root directory of this software component.
* If no LICENSE file comes with this software, it is provided AS-IS.
*
******************************************************************************
*/
/* USER CODE END Header */
#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

void MX_FATFS_Init(void)
{
	/*## FatFS: Link the SD driver ###########################*/
	retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

	/* USER CODE BEGIN Init */
	/* additional user code for init */
	/* USER CODE END Init */
}

/**
* @brief  Gets Time from RTC
*
* Override the weak function in diskio.c
* Provide a pseudo time that changes each call.
*
* DWORD Time format according to Wikipedia
*
* | Bits	 |  Description                                        |
* |----------|-----------------------------------------------------|
* | 31 - 25  | Year. (0 = 1980, theoretically up to 127 = 2107)    |
* | 24 - 21  | Month (1–12)                                        |
* | 20 - 16  | Day (1–31)                                          |
* | 15 - 11  | Hours (0-23)                                        |
* | 10 - 5   | Minutes (0-59)                                      |
* |  4 - 0   | Seconds / 2 (0-29)                                  |
*
* @param  None
* @retval Time in DWORD
*/
DWORD get_fattime(void)
{
	/* USER CODE BEGIN get_fattime */
	static DWORD minutes = 22;
	DWORD time = ((DWORD)(_NORTC_YEAR - 1980) << 25 | (DWORD)_NORTC_MON << 21 | (DWORD)_NORTC_MDAY << 16);
	time |= (((DWORD)(14ul << 11)) /* 14 hours */ );
	minutes += 1;	// pseudo minute tick each call.
	minutes %= 60;
	time |= (minutes << 5);
	// make a pseudo 2 second tick value.
	DWORD seconds = ((HAL_GetTick()>>11ul) & 0x07ful);
	seconds %= 30;
	time |= seconds;
	// Todo: _mg_ Add build option to use time from RTC for get_fattime()
	return time;
	/* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */

/* USER CODE END Application */
